//*******************************************************
// Simulator.java
// created by Sawada Tatsuki on 2018/05/25.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータの抽象クラス */

import java.util.*;

public abstract class AbstractSimulator {
	private int t; //経過ステップ数

	private Vector domainMin; //空間の定義域の下限
	private Vector domainMax; //空間の定義域の下限
	
	protected Random rnd = new Random(); //乱数生成用インスタンス
	
    //コンストラクタ
    public AbstractSimulator() {
		t = 0; //ステップ数を初期化
    }
	
    ///シミュレータを1ステップ動かす
    public void run(){
		process(); //処理を実行
		t++; //ステップ数をカウント
    }
	
    abstract protected void process();	//シミュレータの処理内容: オーバーライド前提
	
	//現在のステップを求める
	public int getStep() {
		return t;
	}

	//定義域を設定・2次元
	protected void setDomain(double xMin, double xMax, double yMin, double yMax) {
		domainMin = new Vector(xMin, yMin); //下限
		domainMax = new Vector(xMax, yMax); //上限
	}
	
	//定義域を設定・3次元
	protected void setDomain(double xMin, double xMax, double yMin, double yMax, double zMin, double zMax) {
		domainMin = new Vector(xMin, yMin, zMin); //下限
		domainMax = new Vector(xMax, yMax, zMax); //上限
	}
}
