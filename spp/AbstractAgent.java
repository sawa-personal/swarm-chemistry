//*******************************************************
// AbstractAgent.java
// created by Sawada Tatsuki on 2018/05/25.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* エージェントの抽象クラス */

//package com.naked_hermit.spp
import java.util.*;

public abstract class AbstractAgent {
	private Vector p; //座標
    private Vector v; //速度
	private Vector vNext; //次ステップの速度
	private double vLim; //最大速度
    private double range; //認識範囲の半径
    private double blind; //死角
	private double blindCos; //視野角に対する余弦
	
    public int id; //エージェントのID
    protected Simulator simulator; //属するシミュレータ
	
    protected static Random rnd = new Random(); //乱数生成用インスタンス
	
    //コンストラクタ
    public AbstractAgent(int id, Simulator simulator) {
		this.id = id; //IDを設定
		this.simulator = simulator; //属するシミュレータを設定
		
		p = new Vector(0, 0); //座標
		v = new Vector(0, 0); //速度
		setCognizance(100, 0); //認識範囲の半径と死角を初期化
		vLim = 5; //最大速度の初期化
    }
	
    //座標を設定 (成分で指定: 2次元)
    public void setPosition(double x, double y) {
		this.p.set(x, y);
    }

	//座標を設定 (成分で指定: 3次元)
    public void setPosition(double x, double y, double z) {
		this.p.set(x, y, z);
    }

	//座標を設定 (ベクトルで指定)
    public void setPosition(Vector q) {
		this.p.set(q.getX(), q.getY(), q.getZ());
    }
	
    //座標を取得
    public Vector getPosition() {
		return p.clone();
    }

    //速度を設定 (成分で指定: 2次元)
    public void setVelocity(double x, double y) {
		this.v.set(x, y);	
    }
	
    //速度を設定 (成分で指定: 3次元)
    public void setVelocity(double x, double y, double z) {
		this.v.set(x, y, z);
    }

    //速度を設定 (ベクトルで指定)
    public void setVelocity(Vector q) {
		this.v.set(q.getX(), q.getY(), q.getZ());
    }
	
    //速度を取得
    public Vector getVelocity() {
		return v.clone();
    }

	//最大速度を設定
	public void setVLim(double vLim) {
		this.vLim = vLim;
	}

	//最大速度を取得
	public double getVLim() {
		return vLim;
	}
	
	//認識 (認識半径と死角) に関する設定
	public void setCognizance(double range, double blind) {
		//例外処理
		if(range < 0 || blind < 0 || blind > 2 * Math.PI) {
			System.exit(1);
		}
		
		this.range = range; //認識範囲を設定
		this.blind = blind; //死角を設定
		this.blindCos = Math.cos(blind / 2); //対応する余弦を設定
	}
	
    //指定のエージェントが認識範囲内か
    public boolean cognize(AbstractAgent opp) {
		Vector toOpp = opp.p.subtract(p);	//自分から相手に向かうベクトル
		double distance = toOpp.norm(); //自分と相手の間の距離
		double cos = Vector.inner(v, toOpp) / (Vector.norm(v) * Vector.norm(toOpp)); //余弦
		return (distance < range && -blindCos <= cos && cos <= 1); //認識範囲内 => true, 認識範囲外 => false
    }
	
	//認識範囲内のエージェント集合を返す
	//【参考】"メソッドの戻り値のジェネリクス（List<String>の"String"部分）を動的に変える" ,https://qiita.com/arai-wa/items/0cefd78f6e4a5b5128dc
	public <T extends AbstractAgent> Set<T> getNeighbors(T[] agent) {
		int num = agent.length; //エージェントの数
		Set<T> neighbors = new HashSet<T>(); //認識範囲内のエージェント集合を初期化
		//認識可能なエージェント
		for(int j = 0; j < num; j++) {
			if(cognize(agent[j])) { //他エージェントjが認識可能なとき
				neighbors.add(agent[j]); //近傍に追加
			}
		}
		return neighbors;		
	}
	
	//自分以外の認識範囲内のエージェント集合を返す
	public <T extends AbstractAgent> Set<T> getNeighborsExceptMe(T[] agent) {
		int num = agent.length; //エージェントの数
		Set<T> neighbors = new HashSet<T>(); //認識範囲内のエージェント集合を初期化
		//認識可能なエージェント
		for(int j = 0; j < num; j++) {
			if(cognize(agent[j]) && j != id) { //他エージェントjが認識可能なとき
				neighbors.add(agent[j]); //近傍に追加
			}
		}
		return neighbors;		
	}

	//認識範囲内のK最近傍集合を返す ★☆★☆★☆★☆★☆ 未実装 ★☆★☆★☆★☆★☆
	public <T extends AbstractAgent> Set<T> getKNN(T[] agent) {
		int num = agent.length; //エージェントの数
		Set<T> neighbors = new HashSet<T>(); //認識範囲内のエージェント集合を初期化
		//認識可能なエージェント
		for(int j = 0; j < num; j++) {
			if(cognize(agent[j]) && j != id) { //他エージェントjが認識可能なとき
				neighbors.add(agent[j]); //近傍に追加
			}
		}
		return neighbors;
	}
	
	//相手から自分に向かうベクトルを求める
	public Vector[][] calcToOwn(AbstractAgent[] agent) {
		int num = agent.length; //エージェントの数
		Vector[][] vectors = new Vector[num][num]; //jからiに向かうベクトルの集合
		
		for(int i = 0; i < num; i++) {
			for(int j = i + 1; j < num; j++) {		
				vectors[i][j] = (agent[i].p).subtract(agent[j].p); //jからiへのベクトル
				vectors[j][i] = vectors[i][j].scalarMultiply(-1); //逆向き
			}
		}
		return vectors;
	}
	
	//回りのエージェントとの距離を求める
	public double[] calcDistance(AbstractAgent[] agent) {
		int num = agent.length; //エージェントの数
		double[] distance = new double[num]; //求めるエージェント間距離
		
		for(int j = 0; j < num; j++) {
			Vector toOwn = p.subtract(agent[j].p); //jからiへのベクトル
			distance[j] = toOwn.norm(); //その長さ
		}

		return distance;
	}
	
	//2種類のエージェント間の距離を求める
	public double[][] calcDistancesBetweenTheTwo(AbstractAgent[] agentA, AbstractAgent[] agentB) {
		int numA = agentA.length; //エージェントaの数
		int numB = agentB.length; //エージェントbの数
		double[][] distanceBetweenTheTwo = new double[numA][numB]; //i番目のエージェントAと，j番目のエージェントBの間の距離
		
		for(int i = 0; i < numA; i++) {
			for(int j = 0; j < numB; j++) {
				Vector bToA = (agentA[i].p).subtract(agentB[j].p); //j(エージェントB)からi(エージェントA)へのベクトル
				distanceBetweenTheTwo[i][j] = bToA.norm(); //その長さ
			}
		}

		return distanceBetweenTheTwo;
	}
	
	//壁で跳ね返す
	protected void rebound(double xMin, double xMax, double yMin, double yMax){
		if(p.getX() > xMax) {
			p.setX(xMax); //位置をxの上限に
			v.setX(-v.getX()); //速度を逆方向に
		} else if(p.getX() < xMin) {
			p.setX(xMin); //位置を領域の端に
			v.setX(-v.getX()); //速度を逆方向に
		}
		if(p.getY() > yMax) {
			p.setY(yMax); //位置を領域の端に
			v.setY(-v.getY()); //速度を逆方向に
		} else if(p.getY() < yMin) {
			p.setY(yMin); //位置を領域の端に
			v.setY(-v.getY()); //速度を逆方向に		
		}	
	}

	//2次元トーラス空間
	protected void torus(double xMin, double xMax, double yMin, double yMax){
		double agentSize = 20; //画面外で隠れる幅
		if(p.getX() > xMax + agentSize) {
			p.setX(xMin - agentSize); //位置を領域の端に
		} else if(p.getX() < xMin - agentSize) {
			p.setX(xMax + agentSize); //位置を領域の端に
		}
		if(p.getY() > yMax + agentSize) {
			p.setY(yMin - agentSize); //位置を領域の端に
		} else if(p.getY() < yMin - agentSize) {
			p.setY(yMax + agentSize); //位置を領域の端に
		}	
	}

	//結合力を求める
	protected <T extends AbstractAgent> Vector getCoh(Set<T> agents) {
		Iterator<T> iter = agents.iterator(); //集合のイテレータ
		ArrayList<Vector> positionSet = new ArrayList<Vector>(); //座標リスト
		while(iter.hasNext()) { //要素を順に読み取る
			T agent = iter.next(); //着目エージェント
			positionSet.add(agent.getPosition()); //座標リストに追加
		}
		
		Vector coh = Vector.average(positionSet); //平均座標
		coh = coh.subtract(p); //平均座標に向かうベクトル
		return coh;
	}
	
	//整列力を求める	
	protected <T extends AbstractAgent> Vector getAlg(Set<T> agents) {
		Iterator<T> iter = agents.iterator(); //集合のイテレータ
		ArrayList<Vector> velocitySet = new ArrayList<Vector>(); //速度リスト
		while(iter.hasNext()) { //要素を順に読み取る
			T agent = iter.next(); //着目エージェント
			velocitySet.add(agent.getVelocity()); //速度リストに追加
		}
		
		Vector alg = Vector.average(velocitySet); //平均速度
		alg = alg.subtract(p); //平均座標に向かうベクトル
		return alg;
	}
	
	//分離力を求める
	protected <T extends AbstractAgent> Vector getSep(Set<T> agents) {
		Iterator<T> iter = agents.iterator(); //集合のイテレータ
		ArrayList<Vector> velocitySet = new ArrayList<Vector>(); //座標リスト

		Vector sep = new Vector(0, 0); //受ける総合力
		
		while(iter.hasNext()) { //要素を順に読み取る
			T agent = iter.next(); //着目エージェント
			Vector toOwn = p.subtract(agent.getPosition()); //相手から自分に向かうベクトル
			double distanceSq = Vector.distanceSq(new Vector(0, 0), toOwn); //二乗ノルム
			toOwn = toOwn.scalarDivide(distanceSq); //二乗ノルムで割る．(距離で割る=斥力を単位ベクトル，さらに距離で割る=斥力は距離に反比例)
			sep = sep.add(toOwn); //総合力に加算
		}
		
		return sep;
	}

	//次ステップの速度を設定
	protected void nextVelocity(Vector vNext) {
		this.vNext = vNext;
	}
	
	//更新フェーズ
	public void act() {
		v = vNext; //速度の更新
		p = p.add(v); //位置の更新
	}
	
	//エージェントの情報を表示
	public void printInfo() {
		System.out.printf("p = (%.2f, %.2f), v = (%.2f, %.2f)\n",
						  p.getX(), p.getY(), v.getX(), v.getY());
	}
}
