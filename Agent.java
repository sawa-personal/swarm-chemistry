//*******************************************************
// Agent.java
// created by Sawada Tatsuki on 2018/05/21.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* エージェントのオブジェクト */

import java.util.*;

public class Agent extends AbstractAgent {
	public static int num = 300; //エージェントの総数
	
	//パラメータ
	private double R; //認識範囲の半径
	private double Vn; //理想的な速度
	private double Vm; //最大速度
	private double c1; //結合力の強さ
	private double c2; //整列力の強さ
	private double c3; //分離力の強さ
	private double c4; //気まぐれに進む確率
	private double c5; //ペースを維持する傾向
	
	//コンストラクタ
	public Agent(int id, Simulator simulator) {
		super(id, simulator); //コンストラクタを継承
		init(); //初期化
	}

	//初期化
	public void init() {
		setCognizance(R, 0); //認識範囲の半径と死角を設定
		//"Blobs": num = 150
		//setParameters(20.8, 1.95, 20.75, 0.95, 0.99, 9.31, 0.05, 0.68);
			
		//"Linear Oscillator": num = 150
		/*
		if(id < 133) {
			setParameters(214.41, 17.93, 35.14, 0.64, 0.13, 0.29, 0.08, 0.97); //パラメータを設定
		} else {
			setParameters(253.6, 7.19, 15.51, 0.82, 0.33, 32.65, 0.34, 0.56); //パラメータを設定
		}
		*/
		
		//"Turbulent Runner": num = 300
		if(id < 131) {
			setParameters(177.1, 9.71, 30.06, 0.8, 0.43, 19.65, 0.45, 0.91);
		} else {
			setParameters(277.3, 14.67, 37.71, 0.68, 0.23, 77.01, 0.02, 0.31);
		}
		
		double interval = simulator.interval;
		setPosition(interval * (rnd.nextDouble() - 0.5), interval * (rnd.nextDouble() - 0.5)); //ランダムな座標
		double r = Math.random() * 2.0 * Math.PI; //ランダムな角度
		double vLim = getVLim();
		setVelocity(vLim * Math.random() * Math.cos(r), vLim * Math.random() * Math.sin(r)); //大きさ一定，方向ランダムな速度
	}
	
	private Set<Agent> neighbors; //認識範囲内のエージェント集合
	//観測フェーズ (全体情報の取得)
	protected void observe() {
		neighbors = getNeighborsExceptMe(simulator.agent); //近傍エージェントを取得
	}

	//行動決定フェーズ
	public void deside() {
		Vector a = new Vector(0, 0); //加速度

		if(neighbors.isEmpty()) { //認識範囲内にエージェントがいない場合はランダムウォーク
			a = new Vector(10 * (rnd.nextDouble() - 0.5), 10 * (rnd.nextDouble() - 0.5)); //迷子
		} else {		
			Vector coh = getCoh(neighbors).scalarMultiply(c1); //結合力 = 座標平均へ向かうベクトル．c1倍に補正
			Vector alg = getAlg(neighbors).scalarMultiply(c2); //整列力 = 速度平均へ向かうベクトル．c2倍に補正
			Vector sep = getSep(neighbors).scalarMultiply(c3); //分離力 = 重心から遠ざかるベクトル．c3倍に補正
				
			a = a.add(coh); //結合力を加算
			a = a.add(alg); //整列力を加算
			a = a.add(sep); //分離力を加算

			if(rnd.nextDouble() < c4) {
				Vector r5 = new Vector(10 * (rnd.nextDouble() - 0.5), 10 * (rnd.nextDouble() - 0.5));
				a = a.add(r5); //気まぐれ
			}
		}

		Vector vNext = getVelocity(); //次ステップの速度
		vNext = vNext.add(a); //加速
		vNext = vNext.constrict(Vm); //速度超過を禁ずる
		Vector vIdeal = vNext.scale(Vn); //理想の速度
		vNext = (vIdeal.scalarMultiply(c5)).add(vNext.scalarMultiply(1 - c5)); //ペース維持
		nextVelocity(vNext); //次ステップの速度を設定
	}
	
	//パラメータの設定
	public void setParameters(double R, double Vn, double Vm, double c1, double c2, double c3, double c4, double c5) {
		this.R = R;
		this.Vn = Vn;
		this.Vm = Vm;
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.c4 = c4;
		this.c5 = c5;
		
		setCognizance(R, 0); //認識範囲の半径と死角を設定
		setVLim(Vm); //最大速度を設定
	}

	public double getC1() {
		return c1;
	}

	public double getC2() {
		return c2;		
	}
	
	public double getC3() {
		return c3;
	}
}
