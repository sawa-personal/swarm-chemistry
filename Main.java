//*******************************************************
// Main.java
// created by Sawada Tatsuki on 2018/01/11.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* Processingによる描画処理 */

import processing.core.*;

public class Main extends PAppletWrapper {
	private int windowSize = 600; //ウィンドウサイズ
	
    public void settings() {
		size(windowSize, windowSize); //ウィンドウサイズを指定
		setTerminate(-1); //終了ステップ数を設定
		doesSave(false); //動画を保存
		setRegulation(windowSize / simulator.interval, new Vector(windowSize / 2.0, windowSize / 2.0)); //拡大比と原点位置を設定
    }

	public void setup() {
		strokeWeight(1.0f); //線の太さを指定
		stroke(64); //枠線の色
		noStroke(); //点の枠線なし
    }

	//描画処理
    public void controller() {
		//frameRate(10); //描画レート
		//overlook(); //俯瞰
		
		t = simulator.getStep(); //経過ステップ数を取得
		background(255);//背景色を指定．画面をリセットする役割もある

		/***** start: エージェントを描画 *****/
		Vector[] position = getPosition(simulator.agent); //座標を取得
		int num = Agent.num; //エージェント数
		
		float[] r = new float[num]; //R
		float[] g = new float[num]; //G
		float[] b = new float[num]; //B

		//色を取得
		for(int i = 0; i < num; i++){
			r[i] = 255 * (float)simulator.agent[i].getC1(); //R
			g[i] = 255 * (float)simulator.agent[i].getC2(); //G
			b[i] = 255 * (float)simulator.agent[i].getC3() / 100; //B
		}
		
		for(int i = 0; i < num; i++){
			fill(r[i], g[i], b[i]); //色を設定
			ellipse((float)position[i].getX(), (float)position[i].getY(), 8, 8); //プロット
		}
		/***** end: エージェントを描画 *****/
		
		fill(40);		
		showCountLabel(t); //カウントラベルを表示
    }
	
    public static void main(String args[]) {
		PApplet.main("Main"); //Mainクラスを呼び出してProcessingを起動
    }
}
