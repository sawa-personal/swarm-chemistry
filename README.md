# swarm-chemistry

Swarm Chemistry のシミュレータ

## 参考文献
* [Hiroki Sayama, D.Sc.: Swarm Chemistry Homepage](http://bingweb.binghamton.edu/~sayama/SwarmChemistry/)
* Hiroki Sayama: ``Swarm chemistry'', Artificial Life 15:105 114, 2009.
