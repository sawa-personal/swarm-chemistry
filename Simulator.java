//*******************************************************
// Simulator.java
// created by Sawada Tatsuki on 2018/05/21.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータのクラス */

public class Simulator extends AbstractSimulator {
	public static int num = Agent.num; //エージェント数
    public Agent agent[] = new Agent[num]; //エージェント
	public static int interval = 600; //空間の1辺の長さ
	
    public Simulator() {
		//エージェントを生成・初期化
		for(int i = 0; i < num; i++) {
			agent[i] = new Agent(i, this);
		}
    }

	//処理の内容
    public void process() {
		//観測フェーズ
		for(int i = 0; i < num; i++) {
			agent[i].observe();
		}
		
		//行動決定フェーズ
		for(int i = 0; i < num; i++) {
			agent[i].deside();
		}

		//次の速度を決定
		for(int i = 0; i < num; i++) {
			agent[i].act();
		}
    }
}
